@extends('layouts.app')

@section('content')
 <!-- Page Content -->
 <div class="container bg-white">

<!-- Page Heading -->
<h1 class="my-5 py-5">Produckte</h1>
<hr>
<ul class="breadcrumb">
  <li><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></i> &nbsp;<a href="#"> Home </a>&nbsp;</li>
  <li><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> &nbsp;<a href="#"> Warme/Kalte </a>&nbsp;</li>
  <li><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> &nbsp;<a href="#"> Automatikfilter </a>&nbsp;</li>
</ul>
<!-- Project One -->
<div class="row">
@foreach($posts as $post)
  <div class="col-md-3 my-4">
    <a href="#">
      <img class="img-responsive" src="{{asset('images/produkt.jpg')}}" alt="">
    </a>
  </div>
  <div class="col-md-8 my-4">
    <h3>{{ $post->title }}</h3>
    <h6> {{ $post->subtitle }}</h6>
    <p>{{ $post->body }}</p>
    <a class="btn btn-primary float-right" href="{{ route('posts.show',['id'=>$post->id]) }}">Mehr Erfahren ></a>
  </div>
  @endforeach  
</div>
<!-- /.row -->

<hr>

</div>
<!-- /.container -->
    <!-- <div class="container">
        <h1>The LRMG Blog</h1>
        <div class="row">  
            @foreach($posts as $post)
                <div class="col-md-12 col-lg-12">
                        <div class="p-3 mb-2 bg-primary text-white">
                            <h2>{{ $post->title }}</h2>
                            <hr>
                            <h6><strong>Post Created At:</strong> {{ date('F d, Y', strtotime($post->created_at)) }}</h6>
                            <p>{{ $post->body }}</p>
                            <a class="btn btn-sm btn-warning" href="{{ route('posts.show',['id'=>$post->id]) }}" role="button">View all</a>    
                        </div>
                </div>
            @endforeach    
        
        </div>
        <div class="text-center">{{ $posts->links() }}</div>
    </div> -->


@endsection
