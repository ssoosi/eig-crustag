<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'EIG CRUSTAG') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    
      
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-light bg-white fixed-top">
      <div class="container">
          <hr>
        <a class="navbar-brand" href="{{url('/')}}"><img src="{{asset('images/logo.png')}}" alt="logo"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
          @guest 
            <li class="nav-item nav-border">
            <a class="nav-link" href="#">Home
                <span class="sr-only">(current)</span>
            </a>
            </li>
            <li class="nav-item nav-border">
            <a class="nav-link" href="#">Uber Uns</a>
            </li>
            <li class="nav-item nav-border active">
            <a class="nav-link" href="#">Produkte</a>
            </li>
            <li class="nav-item nav-border">
            <a class="nav-link" href="#">Referenzen</a>
            </li>
            &nbsp;&nbsp;&nbsp;
            <li class="nav-item navbar-font-lan">
              <a class="nav-link" href="#">DE / FR</a>
            </li>
            @else
            <li><a class="nav-link" href="{{ route('posts.index') }}">Manage posts</a></li>
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        @endguest
          </ul>
        </div>
      </div>
    </nav>
        <main class="py-5">
            @yield('content')
        </main>
    
     <!-- Footer -->
     <footer class="py-5 bg-white">
      <div class="container">
       
       <div class="row">
           
           <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
           © Eig Crustag 2018<br>
           Impressum
           </div>
           
           <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
               <h5>Buro Baar ZG</h3>
               <ul class="list-unstyled footer-ul-list-none">
                   <li><i class="fa fa-mobile" aria-hidden="true"></i>(Tel)+41 22735 95 00</li>
                   <li><i class="fa fa-print" aria-hidden="true"></i>(Fax)+41 786 96 00</li>
                   <li><i class="fa fa-map-marker" aria-hidden="true"></i>Route de Frontenex 120</li>
               </ul>
           </div>
           
           <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
               <h5>Bureau Geneve</h3>
               <ul class="list-unstyled footer-ul-list-none">
                   <li><i class="fa fa-mobile" aria-hidden="true"></i>(Tel)+41 22735 95 00</li>
                   <li><i class="fa fa-print" aria-hidden="true"></i>(Fax)+41 786 96 00</li>
                   <li><i class="fa fa-map-marker" aria-hidden="true"></i>Route de Frontenex 120</li>
               </ul>
           </div>
           
       </div>
       
      </div>
      <!-- /.container -->
    </footer>
</body>
</html>
